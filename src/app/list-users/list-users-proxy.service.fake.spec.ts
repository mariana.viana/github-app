import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { LIST_USERS_FAKE } from './list-users.fake.spec';

export class ListUsersProxyServiceFake {
  constructor() {}

  getUsers(): Observable<HttpResponse<any>> {
    const httpResponse = new HttpResponse<any>({ body: LIST_USERS_FAKE });
    return of(httpResponse);
  }
}
