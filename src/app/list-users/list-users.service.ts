import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { ListUsersProxyService } from './list-users-proxy.service';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})

export class ListUsersService {

  constructor(private proxy: ListUsersProxyService) { }

  getUsers(): Observable<User[]> {
    return this.proxy.getUsers().pipe(
      map(
        response => {
          let listUsers: User[] = [];
          const data = response.body;
          data.forEach((d: { login: any; avatar_url: any; url: any; site_admin: any; }) => {
            const user: User = {
              login: d.login,
              avatar: d.avatar_url,
              url: d.url,
              admin: d.site_admin
            };
            listUsers = [...listUsers, user];
          });
          return listUsers;
        }
      )
    )
  }
}
